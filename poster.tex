\documentclass[landscape, a0]{sciposter}
\usepackage{lipsum}
\usepackage{epsfig}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{multicol}
\usepackage{graphicx,url}
\usepackage[USenglish]{babel}   
\usepackage[utf8]{inputenc}
%\usepackage{fancybullets}
\newtheorem{Def}{Definition}
\definecolor{SectionCol}{rgb}{0,0,0}


\title{Ecosystem Forecast and ENSO Effects}

\author{Forrest Hoffman$\mbox{}^{\dag}$}

\institute 
{$\mbox{}^{\dag}$Climate Change Science Institute \\
 Computational Science and Engineering Division, Oak Ridge National Laboratory, forrest@climatemodeling.org}

%\date is unused by the current \maketitle

%\rightlogo[1]{}
%\leftlogo[0]{}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Begin of Document


\begin{document}
%define conference poster is presented at (appears as footer)
\conference{{\bf NG-Tropics}, Potomac, Maryland, USA}



%\LEFTSIDEfootlogo  
% Uncomment to put footer logo on left side, and 
% conference name on right side of footer

% Some examples of caption control (remove % to check result)

%\renewcommand{\algorithmname}{Algoritme} % for Dutch

%\renewcommand{\mastercapstartstyle}[1]{\textit{\textbf{#1}}}
%\renewcommand{\algcapstartstyle}[1]{\textsc{\textbf{#1}}}
%\renewcommand{\algcapbodystyle}{\bfseries}
%\renewcommand{\thealgorithm}{\Roman{algorithm}}

\maketitle

%%% Begin of Multicols-Enviroment
\begin{multicols}{3}

%%% Abstract

\section{Objectives}

\begin{itemize}
    \item Exploring potentials of E3SM on ecosystem forecasting.
    \item Providing consistent and highly spatiotemporal meteorology forcing data for site-level trait modelings and comparisons.
    \item Studying impacts of ENSO on tropical ecosystems and carbon dynamics as well as climate and biogeochemical extremes.
\end{itemize}


%%% Introduction
\section{Introduction}

The El Ni{\~n}o Southern Oscillation (ENSO) is the most important inter-annual and semi-decadal climate variability and has significant impacts on the global climate through shifting climate patterns and regimes, changing frequencies of climatic and weather extremes, and modulating regional and local water and energy cycles and balances. The impacts and responses of global ecosystem and biosphere to ENSO still remain largely uncertain because:

\begin{itemize}
    \item the responses are highly heterogeneous and depend on their biophysical and biochemical characteristics associated with their plant functional type (PFTs).
    \item there are lack of observations and knowledge gaps of the impacts.
    \item they highly are greatly influenced by the large-scale tele-connections driven by oceanic variability. 
\end{itemize}
 
In this study, we investigated the following subjects using a modeling approach.
\begin{itemize}
    \item the model performance on ecosystem forecast.
    \item the PFT-level response to the ENSO events focusing on two major El Ni{\~n}o events (1997-1998 and 2015-2016).
    \item the impacts of ENSO on the inter-annual variability of atmospheric C$\text{O}_2$ growth rate.
    \item the ENSO induced extremes on the tropical ecosystem.
    \item the oceanic drivers on the terrestrial carbon cycles and extremes.
\end{itemize}

\newcommand{\imsize}{0.55\columnwidth}

%\begin{figure}
%   \begin{center}
%%      \begin{tabular}{c c}
%      \subfigure[]{
%        \includegraphics[width=0.45\columnwidth]{figs/enso-models-jun2015.jpg}}
%         %\includegraphics[width=0.45\columnwidth, %height=0.42\columnwidth]{figs/figure4.png} \\
%      \subfigure[]{
%        \includegraphics[width=0.3\columnwidth, %angle=-90]{figs/oni_amo_dmi.png}}
%%       \end{tabular}
%      \caption{(a) trajectories of Ni{\~n}o index projected by multi-models for 2015-2016 El No{\~n}o event; (b) time evolution of Ni{\~n}o3.4, AMO and DMI indices from 1982 to 2016.}
%    \end{center}
%\end{figure}

\section{Methodology and Models}

\textbf{Energy Exascale Earth System Model(E3SM)} with 1-degree (ne30np4) F-compset configuration simulation: 
\begin{itemize}
    \item active atmosphere model with spectral element dynamic core (CAM5-SE)
    \item active land model with the biogeochemical model turned on; data ocean (DOCN) and thermodynamic sea ice (CICE)
    \item data ocean reads NOAA Optimum Interpolation (OI) version 2 daily sea surface temperature (SST) as well as ice fractions
    \item future SST projections provided by 9-month seasonal forecasts of the NOAA Climate Forecast System (CFSv2); Beyond CFS seasonal forecast period, SSTs and ice fractions are estimated from historical OISSTv2 data till 2020.
\end{itemize}


%\begin{figure}
%    \centering
%    \includegraphics[width=0.45\linewidth]{./figs/schematic_spinup.png}
%    \caption{Caption}
%    \label{fig:my_label}
%\end{figure}

\section{Results and Discussions}


\subsection{PFT-level responses to ENSO}

\begin{figure}
   \renewcommand{\imsize}{0.45\columnwidth}
\begin{center}
   \setlength{\tabcolsep}{0pt}
   \begin{tabular}{c c}
     \includegraphics[width=0.4\linewidth]{./figs/map_regions.pdf} &
     \includegraphics[width=\imsize, height=0.25\linewidth, trim=2cm 1cm 2cm 1cm,clip=true]{./figs/pft_GPP_monthly_regmean_ts_2177-2202.png} \\
     %\includegraphics[width=\imsize, height=0.25\linewidth, trim=2cm 1cm 2cm 1cm, clip=true]{./figs/pft_NPP_monthly_regmean_ts_2177-2202.png}\\
     \includegraphics[width=\imsize, height=0.25\linewidth, trim=2cm 1cm 2cm 1cm, clip=true]{./figs/pft_TLAI_monthly_regmean_ts_2177-2202.png} &
     \includegraphics[width=\imsize, height=0.25\linewidth, trim=2cm 1cm 2cm 1cm, clip=true]{./figs/pft_RSSUN_monthly_regmean_ts_2177-2202.png} \\
   \end{tabular}
\end{center}
\caption{Time evolution of the standardized anomalies of gross primary product (GPP), leaf area index (TLAI), and stomatal resistance on sunlit leaf (RSSUN) for four PFTs (Tropical Broadleaf Evergreen Tree (BET), Tropical Broadleaf Deciduous Tree (BDT), and C3 and C4 grass in five tropical regions from 1995 to 2020. The 1997-1998 and 2014-2016 El Ni\~{n}o events are shaded in a yellow color.}
\end{figure}


\subsection{Effects of ENSO induced extremes on terrestrial ecosystem}

\begin{figure}
\begin{center}
   \subfigure[]{
            \includegraphics[width=0.45\columnwidth, angle=0]{./figs/scPDSI_months_amzgrid.png}}
    \subfigure[]{
            \includegraphics[height=0.35\columnwidth, angle=-90]{./figs/GPP_NEP.png}}
\caption{(a) scPDSI distributions over the ENSO-sensitive grids of the Amazon region (X-axis) and the El Ni{\~n}o months from 1982-2020; (b) percentages of the GPP and NPP losses due to extremes to the absolute values of total losses.}
\end{center}
\end{figure}

\subsection{ENSO impacts on CO2 seasonality and IAV}
\begin{figure}
    \centering
    \subfigure[]{
    \includegraphics[height=0.45\columnwidth, angle=-90]{figs/flask_co2_latitudal_variation.png}}
    \subfigure[]{
    \includegraphics[height=0.45\columnwidth, angle=-90]{figs/iav_co2_ampd.png}}
    \caption{(a) Observed (point) and modeled (line) latitudinal seasonality and IAV of [C$\text{O}_2$]; (b) Observed (blue) and modeled (red) C$\text{O}_2$ seasonality trends over the tropics from 1982 to 2016. Shaded red and blue areas show El Ni{\~n}o and La Ni{\~n}a years respectively}
    \label{fig:my_label}
\end{figure}

\subsection{Oceanic drivers}
   \begin{figure}
         \subfigure[]{%
                \includegraphics[width=0.5\linewidth, trim=2in 0 2in 16.1in, clip=true, angle=0]{./figs/GPP_anom_corr.png}
         }%
         \subfigure[]{
                \includegraphics[width=0.47\linewidth, trim=0in 6in 0in 6in, clip=true, angle=0]{figs/sst_corr2_GPPindamazon.png}
         }
         \caption{(a) correlation coefficients between GPP anomalies of four idealized and control experiments;  (b) correlations between SST and GPP anomalies in the amazon region.}
     \end{figure}

\vspace{-1in}

     \begin{figure}
     \begin{center}
        \subfigure[]{%
                \includegraphics[width=0.32\linewidth, trim=0in 0 0in 0in, clip=true, angle=-90.]{./figs/corr_regions.png}
        }%
        \subfigure[]{
                \includegraphics[width=0.32\linewidth,trim=0.0in 0 0in 0in, clip=true, angle=-90.]{figs/occurrence_gpp_extreme.png}}
        \caption{(a) correlation coefficients of GPP anomalies between four idealized and control experiments; (b) relative changes of occurrences o BGC extremes simulated by four idealized and control experiments.}
     \end{center}
     \end{figure}

\section{Conclusions}
\begin{itemize}
    \item ENSO has large impacts on the tropical ecosystem and the responses of subgrid PFTs to it generally are different.
    \item ENSO induced drought and heat waves affect terrestrial ecosystem severely and approximately 18-43\% total GPP/respiration losses are caused by rare climate extremes.
    \item Modeled CO2 IAV and seasonality are lower and higher than observed ones respectively especially in the northern high latitudes and seasonality on the tropics has a increasing trend since 1997.  
    \item  SST IAVs from the Atlantic and Pacific Oceans are dominant factors responsible for the IAVs of global carbon fluxes and play a nearly equal role on the carbon fluxes in the amazon region.
    \item Without oceanic variability in certain oceans, the simulated drought and GPP extreme occurrences are reduced by 4-91\% and 2-36\% respectively on tropics.

\end{itemize}
%%% References

%% Note: use of BibTeX als works!!

%\bibliographystyle{plain}
%\begin{thebibliography}{1}

%\bibitem{Flusser:Suk:93}
%J.~Flusser and T.~Suk.
%\newblock Pattern recognition by affine moment invariants.
%\newblock {\em Pattern Recognition}, 26:167--174, 1993.
%\end{thebibliography}

\textbf{Acknowledgment:}
This research was supported through as part of the \textbf{Next Generation Ecosystem Experiments-Tropics} and the \textbf{Reducing Uncertainties in Biogeochemical Interactions through Synthesis and Computation} Scientific Focus Area (RUBISCO SFA), which are sponsored by the Climate and Environmental Sciences Division (CESD) of the Biological and Environmental Research (BER) Program in the U.S. Department  of Energy Office of Science. Computational resources were from the National Energy Research Scientific Computing Center, a DOE Office of Science User Facility and the Oak Ridge Leadership Computing Facility at the Oak Ridge National Laboratory, which are supported by the Office of Science of the U.S. Department of Energy under Contract No. \textbf{DE-AC02-05CH11231} and \textbf{DE-AC05-00OR22725} respectively.

\end{multicols}
\end{document}


